$(function() {
		const shopInfoList = [
		{
			genre: 'うなぎ料理専門店・和食',
			shopName: 'うなぎの松重',
			explainText: '鹿児島市下荒田にある昭和21年創業のうなぎ料理専門店。鹿児島県大隅産のうなぎを当店で背開きし、素焼きにした後蒸してから、創業以来受け継がれた秘伝のたれに付け、皮目をしっかりと焼き上げ香ばしく仕上げている。',
			address: '鹿児島市下荒田1-5-10',
			tel: '099-257-2522',
			businessHours: '11:00~14:30(OS)、17:00~20:30(OS)',
			vacation: '木曜(※7月・8月・12月は変更あり)',
		},
		{
			genre: '居酒屋',
			shopName: '地鶏お炭火焼のお店 光太郎',
			explainText: '鹿児島のこだわり地鶏料理が自慢のお店。新鮮な朝引きの鶏刺しや大人気の鶏モモの炭火焼きのほか、	数種類の地鶏料理が味わえるコース、地鶏の2種盛り(モモ、ムネ/880円)、3種盛り(モモ、ムネ、砂ずり/1080円)、4種盛り(モモ、ムネ、砂ずり、レバー/1,480円)、飲み放題もオススメ！',
			address: '鹿児島市加治屋町17-5 フォルシュ加治屋1階',
			tel: '099-295-0200',
			businessHours: '11:30~13:30/17:00~23:00',
			vacation: '日曜',
		},
		{
			genre: '居酒屋',
			shopName: '酒肴 信',
			explainText: '鹿児島中央駅から徒歩5分、地魚、厳選鶏、燻製が当店自慢の『信』。料理も酒類も豊富で、仕事帰りに気軽に立ち寄れる。イチオシメニューは「黒さつま鶏の刺身」。鹿児島の”黒”を背負うかごしま黒豚・鹿児島黒牛、第三の特産品として誕生した黒さつま鶏を刺身でいただける。',
			address: '鹿児島市中央町28-17 西駅ハイツ',
			tel: '099-263-7555',
			businessHours: '12:00~14:00/18:00~24:00',
			vacation: '不定休',
		},
		{
			genre: '居酒屋',
			shopName: '創菜 盛喜',
			explainText: '地魚を使った刺身盛り合わせや黒豚を使用したジューシーで柔らかい肉質の黒豚豚カツ、和牛ホルモン味噌炒めなど素材にこだわったメニューが豊富。人気のさつま揚げは揚げたてのものが食べられるとあって常連さんにも人気のメニュー',
			address: '鹿児島市中央町28-17 西駅ハイツ103',
			tel: '099-252-2727',
			businessHours: '18:00~23:00',
			vacation: '日祝日',
		},
		{
			genre: 'カフェ',
			shopName: 'スイーツカフェオット',
			explainText: 'パティシエが季節のフルーツを贅沢に使って作るスイーツが大人気のカフェ。旬のフルーツを使ったパフェはオールシーズン愛されている看板メニュー。春はイチゴ、初夏はレモンなど、その季節だけの特別なパフェが楽しめる。',
			address: '鹿児島市吉野町7495-6',
			tel: '080-3908-7358',
			businessHours: '11:00~17:00(OS16:00)',
			vacation: '水曜、日祝日',
		},
		{
			genre: '居酒屋',
			shopName: 'さつま馳走 舞田',
			explainText: '鹿児島の老舗で長年修行を積んだ店主が手掛ける、鹿児島の食材を使用した郷土料理が自慢の店。『醤油おでん』(150円～)は玉子・大根などの定番から、鹿児島ならではのさつま揚・黒豚ナンコツなどの変わり種を用意。季節の旬魚や鹿児島県産黒豚を使用した逸品を用意している。',
			address: '鹿児島市中町6-14 さつま屋呉服店ビル4階',
			tel: '099-224-3990',
			businessHours: '11:00~14:30(OS14:00)/18:00~23:30(OS22:30)',
			vacation: '日曜(事前予約の場合は営業)',
		},	
	];
	const gridData = {
		getData: function () {
			//何か配列に変更が起こると反応する配列・・が、'ObservableArray()'
			const shopData = new wijmo.collections.ObservableArray();
			shopInfoList.forEach(shopInfo => shopData.push({
				genre: shopInfo.genre,
				shopName: shopInfo.shopName,
				explainText: shopInfo.explainText,
				address: shopInfo.address,
				tel: shopInfo.tel,
				businessHours: shopInfo.businessHours,
				vacation: shopInfo.vacation,
			}));

			return shopData;
		},
	};

	//FlexGridクラスの新しいインスタンスの初期化
	//第１引数→要素
	//第２引数→中身
	new wijmo.grid.FlexGrid('#gsFlexGrid', {
	    itemsSource: gridData.getData()
	});
})




